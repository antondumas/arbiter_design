
library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;
 
entity arb_tb1 is
end;
 
architecture behavior of arb_tb1 is 
    -- Component Declaration for the Unit Under Test (UUT)
    component arb
    port(clk    : in  std_logic;
         cmd    : in  std_logic;
         rst_n  : in  std_logic;
         req    : in  std_logic_vector(0 to 2);
         N1     : out signed(0 to 1);
         N2     : out signed(0 to 1);
         N3     : out signed(0 to 1);
         gnt    : inout  std_logic_vector(0 to 2)
        );
    end component;
    --Inputs
    signal clk : std_logic := '0';
    signal cmd : std_logic := '0';
    signal rst_n : std_logic := '0';
    signal req : std_logic_vector(0 to 2) := (others => '0');
    --BiDirs
    signal gnt : std_logic_vector(0 to 2);
    signal N1 : signed(0 to 1);
    signal N2 : signed(0 to 1);
    signal N3 : signed(0 to 1);
    -- Clock period definitions
    constant clk_period : time := 10 ns;
begin
	-- Instantiate the Unit Under Test (UUT)
   uut:arb PORT MAP(clk => clk,
                    cmd => cmd,
                    rst_n => rst_n,
                    req => req,
                    N1 => N1,
                    N2 => N2,
                    N3 => N3,
                    gnt => gnt);
   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
   
   cmd_process:process
   begin
        wait for 35ns;
        cmd <= '1';
        wait for 10ns;
        cmd <= '0';
         wait for 5ns;
   end process;
   -- Stimulus process
   stim_proc: process
   begin		
        rst_n <= '0';
        wait for 20ns;
        rst_n <= '1';
        
        wait for 20ns;
        req <= "001";
        
        wait for 50ns;
        req <= "011";
        
        wait for 50ns;
        req <= "111";
        
        wait for 50ns;
        req <= "101";
        
        wait for 50ns;
        req <= "111";
        
        wait for 50ns;
        req <= "011";
        
        wait for 50ns;
        req <= "101";
   end process;

END;
