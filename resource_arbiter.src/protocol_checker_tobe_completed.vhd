library IEEE;
use IEEE.std_logic_1164.all;


entity protocol_checker is
    port(clk, cmd :    in    std_logic;
        req		  :    in    std_logic_vector(0 to 2);
        protocol_violation    :    out   std_logic:='0');
end entity;

architecture bhv of protocol_checker is

	signal violation_1,violation_2 : std_logic;
	signal last_cmd: std_logic:='0';
	signal count : integer := 0;

begin

	process(clk)
	begin
		if clk'event and clk='1' then 
		    -- property 1			
            violation_1 <= '0';
--            if (count = 0 and cmd = '1') then
--                last_cmd <= cmd;
--                count <= 1;
--            elsif (count = 1 and cmd = '1') then
--                count <= 0;
--                if (cmd = last_cmd) then
--                    violation_1 <= '1';
--                end if;
--            end if;
            if (count = 0 and cmd = '1') then
                --last_cmd <= cmd;
                count <= 1;
            elsif (count = 1) then
                count <= 0;
                if (cmd = '1') then 
                    violation_1 <= '1';
                end if;
            end if;
            --Property 2
            violation_2 <= '0'; 
            if (cmd = '1' and req = "000") then
                violation_2 <= '1';
            end if; 
		end if;
	end process;
    -- signal any protocol violation
	protocol_violation <= violation_1 or violation_2;

end bhv;