-- arb_tb.vhd

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity arb_tb2 is
end;

architecture bhv of arb_tb2 is
    signal clk, cmd, protocol_violation, rst_n : std_logic :='0';
    signal req, gnt : std_logic_vector(0 to 2);
    signal fails    : std_logic_vector(0 to 2);
    signal n1,n2,n3 : signed (0 to 1);
    
    component arb
        port(clk   : IN  std_logic;
             cmd   : IN  std_logic;
             rst_n : IN  std_logic;
             req   : IN  std_logic_vector(0 to 2);
             n1    : out signed(0 to 1);
             n2    : out signed(0 to 1);
             n3    : out signed(0 to 1);
             gnt   : INOUT  std_logic_vector(0 to 2)
          );
    end component;
    
    component protocol_checker
        port(clk, cmd : in std_logic;
             req      : in std_logic_vector(0 to 2);
            protocol_violation : out std_logic:='0');
    end component;
    
    component property_checker
        port(clk, cmd : in std_logic;
             req, gnt : in std_logic_vector(0 to 2);
             fails    : out std_logic_vector(0 to 2):="000");
    end component;

    -- Clock period definitions
    constant clk_period : time := 10 ns;
begin    
    
    m_arbiter : arb port map(cmd => cmd, clk => clk, rst_n => rst_n, req=>req, n1=>n1, n2=>n2, n3=>n3, gnt=>gnt);
    m_property_checker : property_checker port map(cmd => cmd, clk => clk, req=>req, gnt=>gnt, fails=>fails);
    m_protocol_checker : protocol_checker port map(cmd => cmd, clk => clk, req=>req, protocol_violation=>protocol_violation);

-- To be completed :
    rst_n <= '1'; --activation des assertions
    clk_process :process
    begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
        --clk <= not(clk) after 10 ns;
    end process;
    
    stim_proc: process
    begin
        req <= "000";
        wait until clk'event and clk='1';
        cmd <= '1';
        wait until clk'event and clk='1';
        cmd <= '0';
        req <= "001";
        wait until clk'event and clk='1';
        cmd <= '0';
        wait until clk'event and clk='1';
        req <= "010";
        --wait until clk'event and clk='1';
        --wait until clk'event and clk='1';
        cmd <= '1';
        wait until clk'event and clk='1';
        cmd <= '1';
        wait until clk'event and clk='1';
        cmd <= '0';
        req <= "000";
        wait until clk'event and clk='1';
        cmd <= '1';
        wait until clk'event and clk='1';
        cmd <= '0';
        req <= "100";
    end process;

--	req <= "000",
--	"001" after 20 ns,
--	"010" after 40 ns,
--	"000" after 60 ns,
--	"100" after 80 ns,
--	"011" after 100 ns,
--	"110" after 120 ns,
--	"000" after 140 ns,
--	"101" after 160 ns,
--	"111" after 180 ns;
        
end bhv;
