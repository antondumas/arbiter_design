-- property_checker.vhd

library IEEE;
use IEEE.std_logic_1164.all;


entity property_checker is
    port(clk, cmd :    in    std_logic;
        req, gnt  :    in    std_logic_vector(0 to 2);
        fails     :    out   std_logic_vector(0 to 2):="000");
end property_checker;

architecture bhv of property_checker is

signal save_req : std_logic_vector(2 downto 0);
signal cmd_pending : std_logic;
signal count : integer := 0;

begin

process(clk)
    begin
        if clk'event and clk='1' then           
           
           if cmd_pending='1' then
               count <= count + 1;
           end if;
           
           if count>=3 then
               count<=0;
               cmd_pending<='0';
           end if;
           if cmd='1' then
               save_req <= req;
               cmd_pending<='1';
               count <= 1;
           end if;
        
        end if;
end process;

process(clk)
    begin
        
    if clk'event and clk='1' then
        --property 0
        if (count=2 and gnt/="001" and gnt/="010" and gnt/="100") then
            fails(0) <= '1';
        else
            fails(0) <= '0';
        end if;
       
        --property 1 : to be completed
       	if ((count=0 or count=3) and gnt/="000") then
		  fails(1) <= '1';
        else
		  fails(1) <= '0';
        end if;
      
        --property 2
        if (count=2 and (save_req and gnt) = "000") then
		  fails(2) <= '1';
        else
		  fails(2) <= '0';
        end if;

        --fails(3) <= '0';
   end if;
end process;

end bhv;
